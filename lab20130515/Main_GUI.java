//It expets as an argument from the command line the dimension of the board
public class Main_GUI{

  public static void main(String[] args){
    Game_15_GUI test=new Game_15_GUI(Integer.parseInt(args[0]));
    test.shuffle();
    int k=0;
    do{
      k=test.get();
      test.swap(k);
    }
    while(!test.is_sorted());
  }

}