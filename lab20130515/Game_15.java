import java.util.Random;
public class Game_15{

  private int[][] board;
  private final int length;

  public Game_15(int l){
    length=l;
    board=new int[length][length];
    int number=1;
    for(int j=0;j<length;j+=1)
      for(int i=0;i<length;i+=1){
        board[i][j]=number;
        number+=1;
        }
    board[length-1][length-1]=-1;
  }

  public boolean is_sorted(){
    boolean sorted=true;
    if(board[length-1][length-1]>0)
      sorted=false;
    if(sorted){
      for(int j=0;j<length;j+=1)
        for(int i=0;i<length-1;i+=1)
          if(board[i][j]>board[i+1][j] && board[i+1][j]>0)
            sorted=false;
      if(sorted)
        for(int j=0;j<length-1;j+=1)
          for(int i=0;i<length;i+=1)
            if(board[i][j]>board[i][j+1] && board[i][j+1]>0)
              sorted=false;
    }
    return sorted;
  }

  public int can_be_moved(int c, int r){
    int mode=0, cv=0, rv=0;
    for(int j=0;j<length;j+=1)
      for(int i=0;i<length;i+=1)
        if(board[i][j]<0){
          cv=i;
          rv=j;
        }
    if(c==cv && r-1==rv)
      mode=1;
    else if(c+1==cv && r==rv)
      mode=2;
    else if(c==cv && r+1==rv)
      mode=3;
    else if(c-1==cv && r==rv)
      mode=4;
    return mode;
  }

  public void swap(int c, int r){
    int possible=can_be_moved(c,r);
    if(possible>0){
      switch(possible){
        case 1:
          board[c][r-1]=board[c][r];
        break;
        case 2:
          board[c+1][r]=board[c][r];
        break;
        case 3:
          board[c][r+1]=board[c][r];
        break;
        case 4:
          board[c-1][r]=board[c][r];
        break;
      }
      board[c][r]=-1;
    }
  }

  public void shuffle(){
    int c, r;
    Random rand=new Random();
    for(int i=0;i<1000;i+=1){
      c=rand.nextInt(length);
      r=rand.nextInt(length);
      swap(c,r);
    }
  }

  public String toString(){
    String s="R0\t|C1|\t|C2|\t|C3|\t|C4| \n";
    for(int j=0;j<length;j+=1){
      s+="R"+(j+1)+"\t";
      for(int i=0;i<length;i+=1)
        s+= "|"+board[i][j]+"|\t";
      s+="\n";
      }
    return s;
  }

}