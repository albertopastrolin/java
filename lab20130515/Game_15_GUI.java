import java.util.Random;
import puzzleboard.PuzzleBoard;
public class Game_15_GUI{

  private int[][] board;
  private final int length;
  private PuzzleBoard gui;

  public Game_15_GUI(int l){
    length=l;
    board=new int[length][length];
    gui=new PuzzleBoard(length);
    int number=1;
    for(int j=0;j<length;j+=1)
      for(int i=0;i<length;i+=1){
        board[i][j]=number;
        if(number<length*length)
          gui.setNumber(j+1,i+1,number);
        number+=1;
        }
    board[length-1][length-1]=-1;
  }

  public boolean is_sorted(){
    boolean sorted=true;
    if(board[length-1][length-1]>0)
      sorted=false;
    if(sorted){
      for(int j=0;j<length;j+=1)
        for(int i=0;i<length-1;i+=1)
          if(board[i][j]>board[i+1][j] && board[i+1][j]>0)
            sorted=false;
      if(sorted)
        for(int j=0;j<length-1;j+=1)
          for(int i=0;i<length;i+=1)
            if(board[i][j]>board[i][j+1] && board[i][j+1]>0)
              sorted=false;
    }
    return sorted;
  }

  public int can_be_moved(int c, int r){
    int mode=0, cv=0, rv=0;
    for(int j=0;j<length;j+=1)
      for(int i=0;i<length;i+=1)
        if(board[i][j]<0){
          cv=i;
          rv=j;
        }
    if(c==cv && r-1==rv)
      mode=1;
    else if(c+1==cv && r==rv)
      mode=2;
    else if(c==cv && r+1==rv)
      mode=3;
    else if(c-1==cv && r==rv)
      mode=4;
    return mode;
  }

  private int getRow(int k){
    int ret=0;
    for(int j=0;j<length;j+=1)
      for(int i=0;i<length;i+=1)
        if(board[i][j]==k)
          ret=j;
    return ret;
  }

    private int getColumn(int k){
    int ret=0;
    for(int j=0;j<length;j+=1)
      for(int i=0;i<length;i+=1)
        if(board[i][j]==k)
          ret=i;
    return ret;
  }

  public void swap(int k){
    swap(getColumn(k),getRow(k));
  }

  public void swap(int c, int r){
    int possible=can_be_moved(c,r);
    if(possible>0){
      switch(possible){
        case 1:
          board[c][r-1]=board[c][r];
          gui.swap(r,c+1,r+1,c+1);
        break;
        case 2:
          board[c+1][r]=board[c][r];
          gui.swap(r+1,c+2,r+1,c+1);
        break;
        case 3:
          board[c][r+1]=board[c][r];
          gui.swap(r+2,c+1,r+1,c+1);
        break;
        case 4:
          board[c-1][r]=board[c][r];
          gui.swap(r+1,c,r+1,c+1);
        break;
      }
      board[c][r]=-1;
      gui.display();
    }
  }

  public void shuffle(){
    int c, r;
    Random rand=new Random();
    for(int i=0;i<1000;i+=1){
      c=rand.nextInt(length);
      r=rand.nextInt(length);
      swap(c,r);
    }
  }

  public int get(){
    return gui.get();
  }

}
