//It expets as an argument from the command line the dimension of the board
import java.util.Scanner;
public class Main{

  public static void main (String[] args){
    Scanner in=new Scanner(System.in);
    Game_15 test=new Game_15(Integer.parseInt(args[0]));
    test.shuffle();
    do{
      System.out.println(test);
      System.out.println("Colonna: ");
      int c=in.nextInt()-1;
      System.out.println("Riga: ");
      int r=in.nextInt()-1;
      test.swap(c,r);
    }
    while(!test.is_sorted());
  }

}