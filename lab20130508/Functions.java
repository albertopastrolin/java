public class Functions{

  public static float f(float n){
    if(n==0)
      return 1;
    else
      return 1 / (1 + f(n - 1));
  }

  public static float fIter(float n){
    return fibo(n) / fibo(n + 1);
  }

  public static float fibo(float n){
    float a = 0, b = 1;
    while (n > 0){
      float pivot = b;
      b += a;
      a = pivot;
      n -= 1;
    }
    return b;
  }


  public static int g(int n, int k){
    if(k==0)
      return 1;
    else
      return (n * g(n-1,k-1)) / k;
  }

  public static int gIter(int n, int k){
    if(n == k)
      return 1;
    else
      if(n < k)
        return 0;
      else
        return fact(n) / fact(k) / fact(n - k);
  }

  public static int fact(int n){
    int result = 1;
    for (int i = n; i > 0; i -= 1)
      result *= i;
    return result;
  }

  public static int h(int n){
    if(n==1)
      return 1;
    else
      return 2 * h(n-1) + n;
  }

  public static int hIter (int n){
    int[] results = new int[n + 1];
    results[1] = 1;
    for (int i = 2; i <= n; i += 1)
      results[i] = 2 * results[i - 1] + i;
    return results[n];
  }

  public static int t(int n){
    if(n==1)
      return 1;
    else
      return 2 * t(n / 2) + n;
  }

  public static int tIter(int n){
    return 2 * (n - 1) + n;
  }

}