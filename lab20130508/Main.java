import java.util.Scanner;
public class Main{
  public static void main(String args[]){
    Scanner in=new Scanner(System.in);
    int n, k;
    /*
    System.out.println("Scrivi n : ");
    float app=in.nextFloat();
    System.out.println("F versione ricorsiva ritorna "+Functions.f(app));
    System.out.println("F versione iterativa ritorna "+Functions.fIter(app));
    System.out.println("Scrivi n : ");
    n=in.nextInt();
    System.out.println("Scrivi k : ");
    k=in.nextInt();
    System.out.println("G versione ricorsiva ritorna "+Functions.g(n,k));
    System.out.println("G versione iterativa ritorna "+Functions.gIter(n,k));
    System.out.println("Scrivi n : ");
    n=in.nextInt();
    System.out.println("H versione ricorsiva ritorna "+Functions.h(n));
    System.out.println("H versione iterativa ritorna "+Functions.hIter(n));
    */
    System.out.println("Scrivi n : ");
    n=in.nextInt();
    System.out.println("T versione ricorsiva ritorna "+Functions.t(n));
    System.out.println("T versione iterativa ritorna "+Functions.tIter(n));
  }
}