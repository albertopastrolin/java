import chessboard.*;
//The chessobard package is required to build the program with a gui
public class Board{

  private int n;
  private int q;
  private int[] r;
  private int[] c;
  private int[] d1;
  private int[] d2;
  private boolean[][] free;
  private boolean existsGui;
  private Chessboard gui;

  public Board(int size, boolean showGui){
  //the constructor require a boolean only because in this way we can reuse this class for both the gui
  //and the calculation of the solution: false to calculate the solutions, and true them
    n=size;
    q=0;
    r=new int [size];
    c=new int [size];
    d1=new int [size*2 -1];
    d2=new int [size*2 -1];
    free=new boolean[size][size];

    for(int i=0; i<size;i+=1){
      r[i]=0;
      c[i]=0;
      d1[i]=0;
      d2[i]=0;
    }
    for(int i=size; i< size*2-1; i+=1){
      d1[i]=0;
      d2[i]=0;
    }
    for(int i=0; i<size; i+=1)
    for(int j=0; j<size; j+=1)
      free[i][j]=true;
    existsGui=showGui;
    if(showGui)
      gui=new Chessboard(size);
  }

  public void showArrangementInGUI(){
    gui.display();
  }

  public int size(){
    return n;
  }

  public int queensOn(){
    return q;
  }

  public void addQueen(int i, int j){
  if(free[i-1][j-1]){
    q+=1;
    r[i-1]+=1;
    c[j-1]+=1;
    d1[i-j+n-1]+=1;
    d2[i+j-2]+=1;
    free[i-1][j-1]=false;
    if(existsGui)
      gui.addQueen(i,j);
  }
  }

  public void removeQueen(int i, int j){
  if(!free[i-1][j-1]){
    q-=1;
    r[i-1]-=1;
    c[j-1]-=1;
    d1[i-j+n-1]-=1;
    d2[i+j-2]-=1;
    free[i-1][j-1]=true;
    if(existsGui)
      gui.removeQueen(i,j);
  }
  }

  public boolean underAttack(int i, int j){
    return (
      (r[i-1]>0)
    ||
      (c[j-1]>0)
    ||
      (d1[i-j+n-1]>0)
    ||
      (d2[i+j-2]>0)
    );
  }

  public int queenInRow(int i){
  int total=0;
  for(int j=0;j<n;j+=1)
    if(!free[i-1][j])
      total+=1;
  return total;
  }

}
