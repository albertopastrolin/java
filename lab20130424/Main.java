import java.util.Scanner;
//In the main we have both the interface for the user and the methods to calculate the solution or to show them into a gui
public class Main{

  public static void arrangements(int n){
    complections(new Board(n,true));
  }

  public static void complections(Board b){
    int q=b.queensOn(), n=b.size(), i=q+1;
    if(q==n)
      b.showArrangementInGUI();
    else
      for(int j=1; j<=n; j+=1)
    if(!b.underAttack(i,j)){
      b.addQueen(i,j);
      complections(b);
      b.removeQueen(i,j);
    }
  }

  public static int numberOfArrangements(int n){
    return numberOfComplections(new Board(n,false));
  }

  public static int numberOfComplections(Board b){
    int q=b.queensOn(), n=b.size(), i=q+1, all=0;
    if(q==n)
      all=1;
    else
    if(b.queenInRow(i)<=1)
    for(int j=1; j<=n; j+=1)
      if(!b.underAttack(i,j)){
        b.addQueen(i,j);
        all+=numberOfComplections(b);
        b.removeQueen(i,j);
      }
    return all;
  }

  public static void main(String[] args){
    Scanner in=new Scanner(System.in);
    int choice;
    do{
      System.out.println("1) Calcola il numero di soluzioni");
      System.out.println("2) Mostra le soluzioni");
      System.out.println("3) Usa una scacchiera con delle regine presenti");
      System.out.println("0) Esci:");
      choice=in.nextInt();
      if(choice>0){
    System.out.print("\nScrivi la dimensione della scacchiera: ");
    int dim=in.nextInt();
    if(choice==1)
      System.out.println("Le soluzioni sono "+numberOfArrangements(dim));
    else
      if(choice==2)
        arrangements(dim);
      else{
        Board userDefined=new Board(dim, false);
        System.out.println("Quante regine vuoi inserire?");
        int queens=in.nextInt();
        while(queens>0){
          System.out.print("I:");
          int i=in.nextInt();
          System.out.print("J:");
          int j=in.nextInt();
          userDefined.addQueen(i,j);
          queens-=1;
        }
        int solutions=numberOfComplections(userDefined);
        System.out.println("La tua Scacchiera ha "+solutions+" soluzioni\n");
      }
      }
    }
    while(choice>0);
  }

}