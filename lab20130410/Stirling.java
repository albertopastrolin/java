public class Stirling{

  public static int secondRec(int n, int k){
    if( k==1 || n==k)
      return 1;
    else
      return secondRec(n-1, k-1) + k * secondRec(n-1, k);
  }

  public static int secondMem(int n, int k, int[][] history){
    if(history[n][k] == UNKNOWN)
      history[n][k]=secondMem(n-1, k-1, history) + k * secondMem(n-1, k, history);
    return history[n][k];
  }

  public static int secondIter(int n, int k){
    int [][] matrix= new int[n+1][k+1];
    for(int j=1; j<=k; j+=1)
    for(int i=1; i<=n; i+=1)
      if(j==1 || j==i)
        matrix[i][j]=1;
      else
        matrix[i][j]=matrix[i-1][j-1] + j * matrix[i-1][j];
    return matrix[n][k];
  }

  public static int second(int n, int k){
    int[][] matrix=new int[n+1][k+1];
    for(int j=1; j<=n; j+=1)
      for(int i=1;i<=k; i+=1)
  if(i==j || i==1)
    matrix[j][i]=1;
  else
    matrix[j][i]=UNKNOWN;
    return secondMem(n,k,matrix);
  }

  public static final int UNKNOWN=-1;

}