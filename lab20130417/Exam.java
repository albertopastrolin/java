public class Exam{

  public static int gRecursive(int x, int y){
    if(x<2 || y<2)
      return x*y;
    else
      return gRecursive(x-1,y) + gRecursive(x,y-2) + gRecursive(x+1,y-1);
  }

  public static int gMemo(int x, int y, int[][] h){
    if(h[x][y] == UNKNOWN)
      h[x][y]=gMemo(x-1,y,h) + gMemo(x,y-2,h) + gMemo(x+1,y-1,h);
    return h[x][y];
  }

  public static int gMemoization(int x, int y){
    int[][] history=new int[(x*y)+1][y+1];
    for(int i=0;i<=x*y; i+=1)
      for(int j=0;j<=y; j+=1)
    if(i<2 || j<2)
      history[i][j]=i*j;
    else
      history[i][j]=UNKNOWN;
    return gMemo(x,y,history);
  }

  public static int gDinamic(int x, int y){
    if ( (x < 2) || (y < 2) )
      return x * y;
    int[][] h = new int[y+1][];
    for ( int i=0; i<=y; i=i+1 ) {
      h[i] = new int[ y+x+1-i ];
      h[i][0] = 0; h[i][1] = i;
    }
    for ( int j=2; j<=y+x; j=j+1 )
      h[0][j] = 0;
    for ( int j=2; j<y+x; j=j+1 )
      h[1][j] = j;
    for ( int i=2; i<=y; i=i+1 )
      for ( int j=2; j<=y+x-i; j=j+1 )
      h[i][j] = h[i][j-1] + h[i-2][j] + h[i-1][j+1];
    return h[y][x];
  }

  public static final int UNKNOWN = -1;

}
