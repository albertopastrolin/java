//A quick interface to test every function of the Lab class from the console
//There aren't any kind of interaction, it is only a batch debug interface
public class Main{
  public static void main (String[] args){

    System.out.print(Lab.gcd(20,40)+"\n");
    System.out.print(Lab.mul(30,2)+"\n");
    IntList test=IntList.NULL;
    for(int i=10; i>=0; i-=1)
      test=IntList.listCons(i,test);
    IntList test2=IntList.NULL;
    System.out.print(test+"\n");
    for(int j=20; j>=11; j-=1)
      test2=IntList.listCons(j,test2);
    System.out.print(test2+"\n");
    System.out.print(Lab.length(test)+"\n");
    System.out.print(Lab.reverse(test)+"\n");
    System.out.print(Lab.append(test,test2)+"\n");
  }

}