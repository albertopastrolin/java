//Conversion of various function from the scheme language to the java language
//The conversion isn't about the solution, that is, as in scheme, recursive
public class Lab{

  public static int gcd (int x, int y){
    if(x==y)
      return x;
    if(x<y)
      return gcd(x, y-x);
    else
      return gcd(x-y, y);
  }

  public static int mul (int m, int n){
    if(n==0)
      return 0;
    if(n%2==0)
      return mul(2*m, n/2);
    else
      return m + mul(2*m, n/2);
  }

  public static int length (IntList list){
    if(IntList.listNull(list))
      return 0;
    else
      return 1 + length(IntList.listCdr(list));
  }

  public static IntList reverse (IntList list){
    if(IntList.listNull(list))
      return list;
    else
      return append (reverse(IntList.listCdr(list)), IntList.listCons (IntList.listCar(list), IntList.NULL));
  }

  public static IntList append (IntList base, IntList list){
    if(IntList.listNull(base))
      return list;
    else
      return IntList.listCons(IntList.listCar(base), append(IntList.listCdr(base), list));
  }

}